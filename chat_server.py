#!usr/bin/python3
import select
import socket
import time
import sys
import argparse
from scapy.all import *

# List to keep track of socket descriptors
CONNECTION_LIST = []

# Flag to show custom prints during dev
DEBUGGER_FLAG = True


'''
    Funcion de print customizada 
'''
def debugger_print(m, flag):

    if flag == True:
        print(m)


def recv_all(sock):
    BUFF_SIZE = 4096
    data = b''
    while True:
        part = sock.recv(BUFF_SIZE)
        data += part
        if len(part) < BUFF_SIZE:
            break

    return data

'''
    Funcion encargada de manejar los paquetes, 
        regist = registrar un nodo nuevo 
        list = enviar la lista de nodos 
        TODO: irc y enviar chat
'''
def packet_manager(pack, mess):
    source = pack[IP].src
    #mess = pack[Raw].load.decode()
    print('Paquete de: ' + source)

    # split the command and the port number
    elem = mess.split("11") #delimitador para el mensaje y puerto no aguata ningun simbolo
    new_port = elem[1] # puerto del que se conecto
    comm = elem[0] # comando enviado

    debugger_print(new_port, DEBUGGER_FLAG)
    debugger_print(comm, DEBUGGER_FLAG)

    # Validate the command
    if comm == 'list':
        print('Solicitando lista de Nodos desde: ' + source + ":"+ new_port)
        soc = socket.socket()
        soc.connect((source, int(new_port)))
        ss = StreamSocket(soc, Raw)
        raw_message = str(CONNECTION_LIST).strip('[]')  # 11 es el delimitador
        ss.send(Raw(raw_message))
        soc.close()

    elif comm == 'regist':
        print('Registrando a: ' + source + ":" + new_port + "\n")
        # global CONNECTION_LIST
        if not (source in CONNECTION_LIST):
            # create a tuple with (host, port)
            new_node = (source, int(new_port)) # to add the ip and the port
            # Add the tuple in a list
            CONNECTION_LIST.append(new_node)
            print('Nodos actualizados: ', CONNECTION_LIST)
        return

    print(mess)
    return


'''
    Funcion principal para inicializar el servidor principal 
'''
def init(serversocket, host, port, conn):
    # Mensajes de bienvenida para el servidor
    print("------------- Bienvenido a la red LISA -------------\n")
    print("----------------- Servidor Central -----------------\n")
    print("Host: " + str(host))
    print("Escuchando en el puerto: " + str(port))
    print("Conexiones permitidas: " + str(conn))
    print("----------------------------------------------------\n")

    while True:
        try:
            connection, address = serversocket.accept()
            # buf = connection.recv(4096)
            buf = recv_all(connection)
            debugger_print(buf, DEBUGGER_FLAG)
            debugger_print(connection, DEBUGGER_FLAG)

            if len(buf) > 0:
                debugger_print(buf, DEBUGGER_FLAG)
                message = buf.decode()
                debugger_print(message, DEBUGGER_FLAG)
                pack = scapy.layers.inet.IP(buf)
                #udp = scapy.layers.inet.TCP(buf) # get the port from they send
                print('------------------------')
                pack.show()
                print('------------------------')
                source = pack[IP].src
                debugger_print(source, DEBUGGER_FLAG)
                packet_manager(pack, message)

        except KeyboardInterrupt:
            print("Deteniendo servidor")
            sys.exit()

if __name__ == "__main__":

    '''
        Obtener los parametro de la terminal

    '''
    parser = argparse.ArgumentParser()
    parser.add_argument("-H", "--host", type=str, help="Host para el servidor")
    parser.add_argument("-p", "--port", type=int, help="Puerto donde estara disponible el server")
    parser.add_argument("-c", "--conections", type=int, help="Cantidad de conexiones disponibles")
    args = parser.parse_args()

    HOST = args.host
    PORT = args.port
    CONN = args.conections

    RECV_BUFFER = 4096  # Advisable to keep it as an exponent of 2

    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # this has no effect, why ?
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server_socket.bind((HOST, PORT))
    server_socket.listen(CONN)

    # Add server socket to the list of readable connections
    # CONNECTION_LIST.append(server_socket)

    # Prevenir el error de los puertos menores a 1024
    if PORT <= 1024:
        print("Debe seleccionar puertos mayores a 1024")
        sys.exit()

    init(server_socket, HOST, PORT, CONN) #iniciar parametros
