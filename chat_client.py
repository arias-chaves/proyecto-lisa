#!/usr/bin/python3
import select
import socket
import string
import sys
import argparse
import threading
from scapy.all import *
import random

# Flag to show custom prints during dev
DEBUGGER_FLAG = True

# Global list to hold the connection list
CONNECTION_HOST = []
CONNECTION_PORT = []

'''
    Funcion de print customizada 
'''
def debugger_print(m, flag):

    if flag == True:
        print(m)

'''
    Funcion para imprimir una lista de tuplas de manera "bonita"
'''
def print_lists(list, list2):
    print("___________Lista de nodos disponibles___________ \n")
    for cont in range(0, len(list)):
        print("        N" + str(cont) + " HOST: " + list[cont] + " PORT: " + list2[cont])
        print("________________________________________________")


'''
    Posible funcion para el chat, sino sirve se quita
'''
def prompt():
    sys.stdout.write('<You> ')
    sys.stdout.flush()


'''
    Funcion para registrar los nodos con el servidor central 
'''
def register_to_server(host, port, server_port):

    print('Registrando en el servidor')
    soc = socket.socket()
    soc.connect((host, server_port))
    ss = StreamSocket(soc, Raw)
    raw_message = 'regist11' + str(port) # 11 es el delimitador
    ss.send(Raw(raw_message))
    soc.close()


'''
    Funcion para solicitar la lista de nodos 
'''
def request_node_list(host, port, server_port):

    print('Solicitando la lista de nodos')
    soc = socket.socket()
    soc.connect((host, server_port))
    ss = StreamSocket(soc, Raw)
    raw_message = 'list11' + str(port)  # 11 es el delimitador
    ss.send(Raw(raw_message))
    soc.close()


'''
    Funcion para probar el envio de paquetes solo para desarrollo
'''
def send_packet_scapy(host, port, server_port):

    s = socket.socket()
    s.connect(('127.0.0.1', 5000))
    ss = StreamSocket(s, Raw)
    ss.sr1(Raw("regist113000"))

    '''
    TIMEOUT = 2
    raw_message = 'regist:' + str(port)
    # pack = Ether() / IP(dst=host) / TCP(dport=5000, sport=3000) / Raw(load=raw_message)
    pack = Ether() / IP(dst=host) / TCP(dport=5000, sport=3000) / raw_message
    pack.show()  # for developped view of pack

    sendp(pack)  # sending usign send from scapy ### NO LLEGA ###
    
    
    ping = IP(dst="www.google.com") / ICMP()
    reply = sr1(ping, timeout=TIMEOUT)
    if not (reply is None):
        print(reply.dst, "is online")
    else:
        print("Timeout waiting for %s" % packet[IP].dst)
    

    print('Paquete enviado')
    print('Tamaño de IP: ', len(pack[IP]))
    print('Tamaño de TCP: ', len(pack[TCP]))
    print('Tamaño de Raw: ', len(pack[Raw]))
    '''

'''
    Funcion para escuchar en el mismo socket, es la funcion de un thread
'''
def listen_connection(host, port):
    try:
        server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server.bind((host, port))
        server.listen(10)

        pack_flag = False
        update_flag = True
        new_route = []

        while True:
            debugger_print('Listen on ' + host + ':' + str(port), DEBUGGER_FLAG)
            connection, address = server.accept()
            buf = connection.recv(2048)
            msg = buf.decode()
            debugger_print(buf, DEBUGGER_FLAG)
            debugger_print(msg, DEBUGGER_FLAG)

            if update_flag is True:
                # update the conection list
                update_connection_list(msg)
                # Create the new circuit
                new_route = create_circiut()
                print(new_route)
                update_flag = False

            input_msg = input("> ")
            print(input_msg)
            send_message(new_route, input_msg)

            if len(buf) > 0 and len(msg) > 0 and pack_flag is True:
                debugger_print("Entre if packet manager", DEBUGGER_FLAG)
                packet_manager(msg)
                pack_flag = True


    except KeyboardInterrupt:
        print("Apagando el nodo")
        # TODO: Delete node
        sys.exit()


'''
    Funcion para manejar los paquetes que recibe el thread 
'''
def packet_manager(mess):

    debugger_print(mess, DEBUGGER_FLAG)
    # split the command and the port number
    # split para mensajes de otro lado no el de la lista que recibio

    elem = mess.split("11")  # delimitador para el mensaje y puerto no aguata ningun simbolo
    comm = elem[0]  # comando enviado
    message = elem[1] # mensaje del chat
    conection_list = elem[2] # lista con los saltos

    debugger_print(comm, DEBUGGER_FLAG)
    debugger_print(message, DEBUGGER_FLAG)
    debugger_print(conection_list, DEBUGGER_FLAG)


    # Validate the command
    if comm == 'ar': #Comando para enviar mensaje por arduino
        print("Enviando mensaje por Arduino")
        #soc = socket.socket()
        #soc.connect((source, int(new_port)))
        #ss = StreamSocket(soc, Raw)
        #raw_message = str(CONNECTION_LIST).strip('[]')  # 11 es el delimitador
        #ss.send(Raw(raw_message))
        #soc.close()

    elif comm == 'cn': #Comando para enviar el msj a clearnet (IRC server)
        print("Enviando mensaje a clearnet")

    elif comm == 'ml': #Comando para enviar el msj por red LISA
        print("Enviando mensaje por red LISA")

        # soc = socket.socket()
        # soc.connect((source, int(new_port)))
        # ss = StreamSocket(soc, Raw)
        # raw_message = 'ml11' + message + '11' + str(conection_list)  # 11 es el delimitador
        # ss.send(Raw(raw_message))
        # soc.close()


    #return


'''
    Funcion para enviar un mensaje a otro nodo
    nodo_list = lista de tuplas  
'''
def send_message(node_list, msg):
    # Si la lista solo tiene un elemento se hace la conexion directa
    if len(node_list) == 1:
        debugger_print("Conexion final", DEBUGGER_FLAG)
        # obtener el host y puerto
        node_host = node_list[0][0]
        node_port = int(node_list[0][1])

        debugger_print(node_host, DEBUGGER_FLAG)
        debugger_print(node_port, DEBUGGER_FLAG)

        soc = socket.socket()
        soc.connect(('127.0.0.1', node_port))
        ss = StreamSocket(soc, Raw)
        raw_message = 'ml11' + str(msg)  # 11 es el delimitador
        ss.send(Raw(raw_message))
        soc.close()

    elif len(node_list) > 1:# Se envia el paquete al siguiente nodo, se envia la lista restante en el paquete
        debugger_print("Conexion intermedia", DEBUGGER_FLAG)

        # Eliminar el primer elemento
        node_list.pop(0)
        head = node_list.pop(0)
        tail = node_list

        debugger_print(head, DEBUGGER_FLAG)
        debugger_print(tail, DEBUGGER_FLAG)

        node_host = head[0]
        node_port = head[1]

        debugger_print(node_host, DEBUGGER_FLAG)
        debugger_print(node_port, DEBUGGER_FLAG)

        soc = socket.socket()
        soc.connect(('127.0.0.1', int(node_port)))
        ss = StreamSocket(soc, Raw)
        raw_message = 'ml11' + str(msg) + '11' + str(tail) # 11 es el delimitador
        ss.send(Raw(raw_message))
        soc.close()



'''
    Funcion para agregar elementos a la lista de conexiones
        "parseando" el string recibido
'''
def update_connection_list(server_msg):
    debugger_print(server_msg, DEBUGGER_FLAG)
    nodes = server_msg.split(',')
    debugger_print(nodes, DEBUGGER_FLAG)

    for cont in range(0, len(nodes)):

        if (cont % 2) == 0:
            new_host = nodes[cont].strip(' ').strip('(')
            debugger_print(new_host, DEBUGGER_FLAG)
            CONNECTION_HOST.append(new_host)
        elif (cont % 2) == 1:
            new_port = nodes[cont].strip(')')
            debugger_print(new_port, DEBUGGER_FLAG)
            CONNECTION_PORT.append(new_port)

    print_lists(CONNECTION_HOST, CONNECTION_PORT)

'''
    Funcion para crear un circuito de conexion con otros nodos
    -> circuito aleatorio
    -> retorna una lista de tuplas 
'''
def create_circiut():
    circuit_list = []

    if len(CONNECTION_HOST) == 1:
        print("Soy el unico en la red")

    elif len(CONNECTION_HOST) == 2:
        print("Solo hay 2 en la red")
        debugger_print("Conexion directa con el otro miembro de la lista", DEBUGGER_FLAG)
        new_node = (CONNECTION_HOST[0], CONNECTION_PORT[0]) #Si solo hay 2 llego de segundo
        circuit_list.append(new_node)

    elif len(CONNECTION_HOST) > 2:
        debugger_print("Mas de 2 elementos en la red", DEBUGGER_FLAG)
        debugger_print("Circuito creado de manera manual", DEBUGGER_FLAG)
        print("Elija la ruta para enviar el mensaje, minimo 3 nodos\n "
              "El primer nodo debe corresponder a su nodo origen\n"
              "El ultimo nodo es el destino.\n")
        print("Separe utilizando coma(,)")
        c = input() # mensaje del teclado
        clist = c.split(',')
        for x in range(0, len(clist)):
            # funciona si el usuario hace lo q se le pide bien
            # TODO: validar los numeros y q sean digitos
            new_node = (CONNECTION_HOST[int(clist[x])], CONNECTION_PORT[int(clist[x])])
            circuit_list.append(new_node)

    return circuit_list

'''
    Funcion principal para inicializar el nodo, 
        registrarse en el servidor, 
        solicitar la lista de nodos, 
        crear el circuito, 
        iniciar el chat. 
'''
def init(host, port, server_port):
    # Mensajes de bienvenida para el servidor
    print("------------- Bienvenido a la red LISA -------------\n")
    print("------------------ Nodo de la red ------------------\n")
    print("Host: " + str(host))
    print("Port: " + str(port))
    print("Well-known Server: " + str(server_port))
    print("----------------------------------------------------\n")

    #registrarse en server
    register_to_server(host, port, server_port) #Función original
    # send_packet_scapy(host, port, server_port) # funcion de pruebas de paquetes

    #solicitar lista
    request_node_list(host, port, server_port)
    #crear ruta

    #iniciar chat


if __name__ == "__main__":

    '''
        Obtener los parametro de la terminal

    '''
    parser = argparse.ArgumentParser()
    parser.add_argument("-H", "--host", type=str, help="Host para el servidor")
    parser.add_argument("-p", "--port", type=int, help="Puerto donde estara disponible el server")
    args = parser.parse_args()

    HOST = args.host
    PORT = args.port
    SERVER_PORT = 5000 # Puerto bien conocido del servidor central

    # Prevenir el error de los puertos menores a 1024
    if PORT <= 1024:
        print("Debe seleccionar puertos mayores a 1024")
        sys.exit()


    # inicializar el seteo de informacion
    init(HOST, PORT, SERVER_PORT)

    # Iniciar un hilo que se encargue de escuchar en el puerto ingresado por parametros
    listen = threading.Thread(target=listen_connection(HOST, PORT), name="Listen")
    listen.start()

