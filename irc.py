#!/usr/bin/python3
import socket
#----------------------------------------------- Variables Globales-----------------------------------------------------------------
ircsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server = "10.0.2.15" # Server conocido. tiene que ser el mismo que se usa en LISA
#Para el testeo se utilizara un canal customizado y asi evitar que hayan mas usuarios.
channel = "##bot-testing" # Channel
botnick = "IamaLISABot" # Your bots nick.
adminname = "LISA"
exitcode = "bye " + botnick #Text that we will use
#----------------------------------------------- Conexion IRC -----------------------------------------------------------------

ircsock.connect((server, 6667)) # Here we connect to the server using the port 6667
ircsock.send(bytes("USER "+ botnick +" "+ botnick +" "+ botnick + " " + botnick + "\n", "UTF-8")) # user information
ircsock.send(bytes("NICK "+ botnick +"\n", "UTF-8"))

#----------------------------------------------- Funciones IRC -----------------------------------------------------------------

def joinchan(chan): # FUNCION PARA CONECTARNOS AL CANAL EN EL SERVIDOR

  ircsock.send(bytes("JOIN "+ chan +"\n", "UTF-8"))
  ircmsg = ""
  while ircmsg.find("End of /NAMES list.") == -1:
    ircmsg = ircsock.recv(2048).decode("UTF-8")
    ircmsg = ircmsg.strip('\n\r')
    print(ircmsg)

def ping(): # Saber que hay conexion entre nodos
  ircsock.send(bytes("PONG :pingis\n", "UTF-8"))

def sendmsg(msg, target=channel): #enviar mensaje al canal
  ircsock.send(bytes("PRIVMSG "+ target +" :"+ msg +"\n", "UTF-8"))

def main():
  joinchan(channel)

  while 1:

    ircmsg = ircsock.recv(2048).decode("UTF-8")
    ircmsg = ircmsg.strip('\n\r')
    #This will print the received information to your terminal. You can skip this if you don’t want to see it, but it helps with debugging and to make sure your bot is working.
    print(ircmsg)
    if ircmsg.find("PRIVMSG") != -1:
      #Messages come in from from IRC in the format of ":[Nick]!~[hostname]@[IP Address] PRIVMSG [channel] :[message]”
      #We need to split and parse it to analyze each part individually.
      name = ircmsg.split('!',1)[0][1:]
      message = ircmsg.split('PRIVMSG',1)[1].split(':',1)[1]
      if len(name) < 17:

        if message.find('Hi ' + botnick) != -1:

         if message[:5].find('.tell') != -1:#Aqui se puede usar public
          target = message.split(' ', 1)[1]
          #After that, we make sure the rest of it is in the correct format. If there is not another then we don’t know where the username ends and the message begins!
          if target.find(' ') != -1:
              message = target.split(' ', 1)[1]
              target = target.split(' ')[0]
          else:
            target = name
            message = "Could not parse. The message should be in the format of ‘.tell [target] [message]’ to work properly."
          sendmsg(message, target)
        if name.lower() == adminname.lower() and message.rstrip() == exitcode:
          sendmsg("oh...okay. :'(")
          #Send the quit command to the IRC server so it knows we’re disconnecting.
          ircsock.send(bytes("QUIT \n", "UTF-8"))
          return
    #If the message is not a PRIVMSG it still might need some response.
    else:
      #Check if the information we received was a PING request. If so, we call the ping() function we defined earlier so we respond with a PONG.
      if ircmsg.find("PING :") != -1:
        ping()
#Finally, now that the main function is defined, we need some code to get it started.
main()
