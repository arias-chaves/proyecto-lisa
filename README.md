# proyecto-LISA

##### 1er proyecto del curso de redes del ITCR, II semestre 2019.
Profesor: Kevin Moraga García

Alumnos:
* Luis Antonio Arias
* Iván Quesada
* Daniela Quesada

#### Prerequisitos
+ Tener instalado Python 3 en su computadora.
+ Instalar [Scapy](https://scapy.readthedocs.io/en/latest/installation.html) en la computadora.

#### Ejecución de chat_server.py
```python3 chat_server.py -H <host> -p 5000 -c <cant. conexiones> ```

El host corresponde a la computadora donde se esté ejecutando. Y el puerto puede variar pero para afectos de este proyecto es un puerto bien conocido 5000.

#### Ejecución de chat_server.py
```sudo python3 chat_client.py -H <host> -p <puerto> ```

En este caso debe ejecutarse con sudo por permisos que necesita la biblioteca scapy para enviar paquetes. Se puede elegir el puerto que se desea.
