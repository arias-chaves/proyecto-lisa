const int ledPin = 12;
const int ledPin_Env = 11;
int fotosensor_A = 0;
int mensaje = 0; 

void setup() {
  // put your setup code here, to run once:
  pinMode(ledPin, OUTPUT);
  pinMode(ledPin_Env, OUTPUT);
  Serial.begin(9600);
}

void loop() {
  //digitalWrite(ledPin,HIGH);
  // --------------------------------------- Recibir paquetes binarios
  int lectura_A = analogRead(fotosensor_A);
  //Serial.println(lectura_A);
  if (lectura_A >= 100){
    Serial.print('1');
  }
  if (lectura_A < 100){
    Serial.print('0');
  }
  delay(500);
  // --------------------------------------- Enviar paquetes binarios
  if (Serial.available() > 0){
    mensaje = Serial.read();
    if (mensaje == 'H'){
      digitalWrite(ledPin,HIGH);
      digitalWrite(ledPin_Env,HIGH);
    }
    if (mensaje == 'L'){
      digitalWrite(ledPin,LOW);
      digitalWrite(ledPin_Env,LOW);
    }
  }
}
