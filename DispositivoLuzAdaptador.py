import binascii
import serial
import time

# ***********************************************************************Función para leer la trama
def _lectura_trama(hilera):
    lista = []
    dato = ''
    for elemento in hilera:
        if elemento == '?':
            lista.append(dato)
            dato = ''
        elif elemento != '<' or elemento != '>' or elemento != '?':
            dato+=elemento
    return lista

# ***********************************************************************Función para crear la tabla
def _crear_trama(_dato, _ip_dest, _puerto_s, _puerto_p):
    trama = '<' + _ip_dest + '?' + _puerto_s + '?' + _puerto_p + '?' + _dato + '>'
    return trama    

# ***********************************************************************Función para calcular checksum
def _calc_checksum(dato):
    return '%2X' % (-(sum(ord(c) for c in dato) % 256) & 0xFF)

# ***********************************************************************Función para convertir
def _convertir_bin(text, encoding='utf-8', errors='surrogatepass'):  # --------------------------- String a Binario
    bits = bin(int(binascii.hexlify(text.encode(encoding, errors)), 16))[2:]
    return bits.zfill(8 * ((len(bits) + 7) // 8))

def _convertir_string(bits, encoding='utf-8', errors='surrogatepass'): # --------------------------- Binario a String
    n = int(bits, 2)
    return _int2bytes(n).decode(encoding, errors)

def _int2bytes(i):
    hex_string = '%x' % i
    n = len(hex_string)
    return binascii.unhexlify(hex_string.zfill(n + (n & 1)))

# ***********************************************************************Función para enviar datos
def enviar_datos(hilera):
    _bin = _convertir_bin(hilera)
    _enviar_datos_aux(str(_bin))
 
def _enviar_datos_aux(binario):
    ser_enviar = serial.Serial('COM14', 9600)
    ser_enviar.write(b'L')
    for bit in binario:
        if bit == '1':
            ser_enviar.write(b'H')
            time.sleep(0.50)
        else:
            ser_enviar.write(b'L')
            time.sleep(0.50)
    ser_enviar.write(b'L')
    ser_enviar.close()
    return 0

# ***********************************************************************Función para recibir datos
def recibir_datos():
    ser_recibir = serial.Serial('COM12', 9600)
    binario = ''
    bit = ''
    bandera = False
    cont = 0
    while True:
        bit += str(ser_recibir.read().decode())
        time.sleep(0.25)
        if bit == '1':
            if bandera == False:
                bandera = True
            else:
                binario += '1'
                binario += bit
                cont += 1
        else:
            if bandera == True:
                binario += bit
                cont += 1

        if cantidad_bits == cont:
            break
    
    return binario